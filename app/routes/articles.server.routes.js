'use strict';

var users = require('../../app/controllers/users.server.controller'),
    articles= require('../../app/controllers/articles.server.controller');

module.exports = function (app) {
    app.route('/api/articles')
        .get(articles.list)
        .post(users.requiresLogin, articles.create);

    app.route('/api/articles/:articleId')
        .get(articles.read)
        .put(articles.update)
        .delete(users.requiresLogin, articles.hasAuthorization, articles.delete);

    app.route('/test')
        .get(users.requiresLogin, function (req, res, next) {
            console.log(req);
            res.send('FROM SECOND');
        });

    app.param('articleId', articles.articleByID);
};
