'use strict';

var app = require('../../server.js'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Article = mongoose.model('Article');

var user, article;

describe('Aricle Model Unit Test:', function () {
    beforeEach(function(done) {
        user = new User({
            firstName: 'John',
            lastName: 'Doe',
            displayName: 'John Doe',
            email: 'jdoe@test.com',
            username: 'jdoe',
            password: 'password'
        });

        user.save( function () {
            article = new Article({
                title: 'Article Title',
                content: 'Article Content',
                user: user
            });
            done();
        });
    });

    describe('Testing the save method', function() {
        it('Should be able to save without problems', function() {
            article.save( function (err) {
                expect(err).toBe(null);
            });
        });

        it('Should not be able to save an article without a title', function() {
            article.title = '';

            article.save( function (err) {
                expect(err).toBeDefined();
            });
        });

    });

    afterEach(function(done) {
        Article.remove( function () {
            User.remove( function () {
                done();
            });
        });
    });
});
