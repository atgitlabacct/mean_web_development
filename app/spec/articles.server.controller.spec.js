'use strict';

var app = require('../../server'),
    request = require('supertest'),
    mongoose = require('mongoose'),
    User = mongoose.model('User'),
    Article = mongoose.model('Article');

var user, article;

describe('Articles Controller Unit Tests:', function() {
    beforeEach(function(done) {
        user = new User({
            firstName: 'John',
            lastName: 'Doe',
            displayName: 'John Doe',
            email: 'jdoe@example.com',
            username: 'jdoe',
            password: 'password'
        });

        user.save( function () {
            article = new Article({
                title: 'Article Title',
                content: 'Article Content',
                user: user
            });

            article.save( function (err) {
                done();
            });
        });
    });

    describe('Testing the GET methods', function() {
        it('Should be able to get the list of articles', function(done) {
            request(app).get('/api/articles/')
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end( function (err, res) {
                    expect(res.body).toEqual(jasmine.any(Array));
                    expect(res.body.length).toEqual(1);
                    expect(res.body[0].title).toEqual(article.title);
                    expect(res.body[0].content).toEqual(article.content);

                    done();
                });
        });

        it('Should be able to get the specific article', function(done) {
            request(app).get('/api/articles/' + article.id)
                .set('Accept', 'application/json')
                .expect('Content-Type', /json/)
                .expect(200)
                .end( function (err, res) {
                    expect(res.body).toEqual(jasmine.any(Object));
                    expect(res.body.title).toEqual(article.title);
                    expect(res.body.content).toEqual(article.content);

                    done();
                });
        });
    });

    afterEach(function(done) {
        Article.remove().exec();
        User.remove().exec();
        done();
    });

});
