process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var mongoose = require('./config/mongoose'),
    passport = require('./config/passport'),
    express = require('./config/express');

var db = mongoose();
var app = express(db);
var passport = passport();

app.listen(3000);

module.exports = app;

console.log('Server running at http://localhost:3000');
