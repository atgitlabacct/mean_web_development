'use strict';

angular.module('articles').controller('ArticlesController', 
        ['$scope', '$routeParams', '$location', 'Authentication', 'Articles', 
        function ($scope, $routeParams, $location, Authentication, Articles) {
            $scope.authentication = Authentication;

            $scope.create = function () {
                var article = new Articles({
                    title: this.title,
                    content: this.content
                });

                article.$save( function (response) {
                    $location.path('articles/' + response._id);
                }, function (errorResponse) {
                    $scope.error = errorResponse.data.message;
                });
            };

            $scope.find = function () {
                $scope.articles = Articles.query();
            };

            $scope.findOne = function () {
                $scope.article = Articles.get({
                    articleId: $routeParams.articleId
                });
            };

            $scope.update = function () {
                console.log('============== update ====================');
                console.log($scope.article);
                $scope.article.$update(function success() {
                    console.log('success');
                    $location.path('/#!/articles/' + $scope.article._id);
                }, function error(errorResponse) {
                    console.log(errorResponse);
                    $scope.error = errorResponse.data.mesage;
                });
            };

            $scope.delete = function (article) {
                if (article) {
                    console.log(article);
                    article.$remove(function () {
                        for (var i in $scope.articles) {
                            if ($scope.articles[i] === article) {
                                $scope.articles.splice(i, 1);
                            }
                        }
                    });
                } else {
                    $scope.article.$remove(function () {
                        $location.path('/#!/articles');
                    });
                }
            };
        }]);
