'use strict';

describe('Testing Articles Controller', function() {
    var _scope, ArticlesController, $controller;

    beforeEach(function() {
        module('mean');
        module('articles');
        module('users');
    });

    beforeEach(function() {

        jasmine.addMatchers({
            toEqualData: function (util, customeEqualityTesters) {
                return {
                    compare: function (actual, expected) {
                        return {
                            pass: angular.equals(actual, expected)
                        };
                    }
                };
            }
        });
    });

    beforeEach(inject(function ($rootScope, _$controller_)  {
        _scope = $rootScope.$new();
        $controller = _$controller_;
        ArticlesController = $controller('ArticlesController', {
            $scope: _scope,
        });
    }));

    it('Should have a find method that uses $resource to retrieve a list of articles', inject(function(Articles) {
        inject( function ($httpBackend) {
            var sampleArticle = new Articles({
                title: 'An Article about MEAN',
                content: 'MEAN rocks!'
            });

            var sampleArticles = [sampleArticle];

            $httpBackend.expectGET('/api/articles').respond(sampleArticles);

            _scope.find();
            $httpBackend.flush();

            expect(_scope.articles).toEqualData(sampleArticles);
        });
    }));

    it('Should have a findOne method that uses $resource to retreive a single of article', 
            inject(function(Articles) {
                inject(function ($httpBackend, $routeParams) {
                    var sampleArticle = new Articles({
                        title: 'An article about MEAN',
                        content: 'MEAN rocks!'
                    });

                    $routeParams.articleId = 'abcdef123456789012345678';

                    $httpBackend.expectGET(/api\/articles\/([0-9a-fA-F]{24})$/).respond(sampleArticle);
                    _scope.findOne();
                    $httpBackend.flush();

                    expect(_scope.article).toEqualData(sampleArticle);
                });
            }));
});
